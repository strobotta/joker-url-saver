#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
php -S localhost:3001 -t ${DIR}/public \
  -d xdebug.mode=debug \
  -d xdebug.var_display_max_depth=100 \
  -d xdebug.idekey=xdebug \
  -d xdebug.client_host=127.0.0.1 \
  -d xdebug.client_port=9003 \
  -d xdebug.start_with_request=yes \
  -d xdebug.discover_client_host=1