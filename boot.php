<?php

// Autoloader for the vendor directory.
require_once implode(DIRECTORY_SEPARATOR, [dirname(__FILE__), 'vendor', 'autoload.php']);

// Autoloader for the App Namespace.
spl_autoload_register(function($className) {
    if (strpos($className, 'App\\') === 0) {
        $file = str_replace('\\', DIRECTORY_SEPARATOR, substr($className, 4)) . '.php';
        require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR . $file;
    }
});

define('APP_BASE_DIR', dirname(__FILE__));

\App\Config::create([
    'data_dir' => APP_BASE_DIR . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR,
    'db_adapter' => '\\App\\Db\\JsonDb',
]);
