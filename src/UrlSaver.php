<?php

namespace App;

use Graby\Graby;
use GuzzleHttp\Client;
use Masterminds\HTML5;

class UrlSaver
{
    /**
     * The URL that should be fetched to save the content.
     * @var string
     */
    protected $url;

    /**
     * List of images that are saved apart from the url itself but are referenced
     * in the HTML of the page.
     * @var array
     */
    protected $images;

    /**
     * The result array with information about the url (e.g. HTML headers).
     * @var array
     */
    protected $result;

    /**
     * Custom temp dir below the system temp where the images are saved to.
     * @var string
     */
    private $tempDir;

    /**
     * Constructor, set the url that should be fetched.
     *
     * @param string $url
     */
    public function __construct(string $url) {
        $this->url = $url;
    }

    /**
     * Fetch the content from the url.
     * @return UrlSaver
     */
    public function fetch(): self {
        $graby = new Graby();
        $this->result = $graby->fetchContent($this->url);
        $this->fetchImages();
        return $this;
    }

    /**
     * Return the result array after the fetch attempt.
     * @return array
     */
    public function getResult(): array
    {
        return $this->result ?? [];
    }

    /**
     * Check function whether the fetch attempt was successful or not.
     * @return bool
     */
    public function success(): bool
    {
        return (\is_array($this->result) &&
            $this->result['status'] === 200 &&
            !empty($this->result['html'])
        );
    }

    /**
     * Return the fetched html string.
     * @return string
     */
    public function getHtml(): string
    {
        return $this->result['html'] ?? '';
    }

    /**
     * Parse the HTML for <picture> elements, fetch the referenced image, store it locally
     * and replace the url with the local one. The reference here goes to the temp dir and
     * must be corrected once the object is persisted permanent.
     *
     * @return UrlSaver
     * @throws \DOMException
     */
    public function fetchImages(): self
    {
        if (!$this->success()) {
            return $this;
        }
        $this->images = [];
        $html5 = new HTML5();
        $dom = $html5->loadHTML($this->getHtml());
        $pictureList = $dom->getElementsByTagName('picture');
        if ($pictureList->count() === 0) {
            foreach ($dom->getElementsByTagName('img') as $image) {
                $src = $image->getAttribute('src');
                if ($this->loadImageFromUrl($src)) {
                    $cnt = count($this->images) - 1;
                    $image->setAttribute('src', $this->images[$cnt]['fs']);
                }
            }
        }
        foreach ($pictureList as $picture) {
            if ($picture->getElementsByTagName('img')->count() > 0) {
                continue;
            }
            $replaceImg = $replaceAnchor = null;
            foreach ($picture->getElementsByTagName('source') as $source) {
                $imgsrc = $source->getAttribute('srcset');
                $imgList = [];
                foreach (explode(',', $imgsrc) as $img) {
                    [$url, $size] = explode(' ', trim($img));
                    $imgList[(int)$size] = $url;
                }
                if (!empty($imgList)) {
                    \ksort($imgList);
                    $smallest = \array_key_first($imgList);
                    if ($this->loadImageFromUrl($imgList[$smallest])) {
                        $cnt = count($this->images) - 1;
                        $replaceImg = $dom->createElement('img');
                        $replaceImg->setAttribute('src', $this->images[$cnt]['fs']);
                        $replaceImg->setAttribute('alt', '');
                        $replaceImg->setAttribute('style', "width: {$smallest}px; height: auto;");
                    }
                    if (count($imgList) > 1) {
                        $largest = \array_key_last($imgList);
                        if ($this->loadImageFromUrl($imgList[$largest])) {
                            $cnt = count($this->images) - 1;
                            if ($replaceImg) {
                                $replaceAnchor = $dom->createElement('a');
                                $replaceAnchor->setAttribute('href', $this->images[$cnt]['fs']);
                                $replaceAnchor->appendChild($replaceImg);
                            } else {
                                $replaceImg = $dom->createElement('img');
                                $replaceImg->setAttribute('src', $this->images[$cnt]['fs']);
                                $replaceImg->setAttribute('alt', '');
                                $replaceImg->setAttribute('style', "width: {$largest}px; height: auto;");
                            }
                        }
                    }
                }
                if (!empty($replaceAnchor)) {
                    $source->parentNode->replaceChild($replaceAnchor, $source);
                    break;
                }
                if (!empty($replaceImg)) {
                    $source->parentNode->replaceChild($replaceImg, $source);
                    break;
                }
            }
        }
        // Because of the dom we have a complete document with header and body. Therefore,
        // Select the <html> node and all inner content, and remove the opening and closing tags.
        $newHtml = trim(str_replace(['<!DOCTYPE html>', '<html>', '</html>'], '', $html5->saveHTML($dom)));
        $this->result['html'] = $newHtml;
        return $this;
    }

    /**
     * Load image from a given url. Determine type and store the image in the subfolder of this item.
     * @param string $url
     * @return bool
     */
    protected function loadImageFromUrl(string $url): bool
    {
        $client = new Client();
        try {
            $res = $client->get($url);
        } catch (\Exception $e) {
            error_log('Could not retrieve image from ' . $url . ': ' . $e->getMessage());
            return false;
        }
        $blob = $res->getBody()->getContents();
        $contentType = $res->getHeader('Content-Type');
        if (\is_array($contentType) && !empty($contentType)) {
            $contentType = $contentType[0];
        }
        $contentLength = $res->getHeader('content-length');
        if (\is_array($contentLength) && !empty($contentLength)) {
            $contentLength = $contentLength[0];
        }
        [$type, $ext] = explode('/', \is_string($contentType)
            ? $contentType: Util::guessContentType($url));
        if ($type === 'image' && $ext) {
            if ($ext === 'svg+xml') {
                $ext = 'svg';
            }
            $name = $this->getTempDir() . DIRECTORY_SEPARATOR . 'img' . count($this->images) . '.' . $ext;
            file_put_contents($name, $blob);
            $this->images[] = [
                'url' => $url,
                'fs' => $name,
                'type' => $contentType,
                'size' => $contentLength,
            ];
            return true;
        }
        return false;
    }

    /**
     * Get (create it when not existent) the temporary directory where to store the downloaded images.
     * @return string
     */
    public function getTempDir(): string
    {
        if (!$this->tempDir) {
            $this->tempDir = sys_get_temp_dir() . DIRECTORY_SEPARATOR
                . preg_replace('~[^\w]~', '_', __CLASS__ . microtime());
            if (!is_dir($this->tempDir)) {
                mkdir($this->tempDir, 0755);
                if (!is_dir($this->tempDir)) {
                    throw new \RuntimeException('could not create temp dir: ' . $this->tempDir);
                }
            }
        }
        return $this->tempDir;
    }

    /**
     * Return a list of fetched images (meta data, not the blob itself).
     * @return array
     */
    public function getImages(): array
    {
        return $this->images ?? [];
    }

    /**
     * Return a list of downloaded images (their location on the temp file system).
     * @return array
     */
    public function getImageList(): array
    {
        $images = [];
        foreach ($this->images ?? [] as $img) {
            $images[] = $img['fs'];
        }
        return $images;
    }
}