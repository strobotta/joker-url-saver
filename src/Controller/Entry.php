<?php

namespace App\Controller;

use App\Config;
use App\Data\Entry as EntryModel;
use App\Data\EntryHtml as EntryHtmlModel;
use App\Util;

class Entry extends Base
{

    public function create()
    {
        $entry = new EntryModel();
        $entry->url = $this->f3->get('POST.url');
        if (filter_var($entry->url, FILTER_VALIDATE_URL)) {
            if ($entry->fetchFromUrl()) {
                $entry->persist();
                $this->f3->reroute('@home');
                return;
            }
            echo '<div style="color: red">Could not fetch url</div>';
            return;
        }
        echo '<div style="color: red">No valid url given</div>';
    }

    public function html()
    {
        $entry = Config::getDbAdapter()->getEntityById(EntryModel::class,
            $this->f3->get('PARAMS.id'));
        if ($entry) {
            Util::view('entry-html', [
                'html' => $entry->getHtml()->getHtml(),
                'title' => $entry->getTitle(),
            ]);
            return;
        }
        $this->f3->error(404, 'Entry not found');
    }

    public function img()
    {
        $imgFile = Config::getDataDir() . $this->f3->get('PARAMS.id')
            . DIRECTORY_SEPARATOR . $this->f3->get('GET.file');
        if (!file_exists($imgFile) || !is_file($imgFile)) {
            $this->f3->error(404, 'File not found');
        }
        $entry = Config::getDbAdapter()->getEntityById(EntryHtmlModel::class,
            $this->f3->get('PARAMS.id'));
        $headersSend = false;
        if ($entry && !empty($entry->getImages())) {
            foreach ($entry->getImages() as $image) {
                if (basename($image['fs']) === $this->f3->get('GET.file')) {
                    header('Content-Type: ' . $image['type']);
                    header('Content-length: ' . $image['size']);
                    $headersSend = true;
                    break;
                }
            }
        }
        if (!$headersSend) {
            header('Content-Type: ' . Util::guessContentType($imgFile));
            header('Content-length: ' . filesize($imgFile));
        }
        $fp = fopen($imgFile, 'rb');
        fpassthru($fp);
        exit;
    }

    public function url()
    {
        $entry = Config::getDbAdapter()->getEntityById(EntryModel::class,
            $this->f3->get('PARAMS.id'));
        if ($entry && $entry->url) {
            $this->f3->reroute($entry->url);
            return;
        }
        $this->f3->error(404, 'Entry not found');
    }

    public function delete()
    {
        $entry = Config::getDbAdapter()->getEntityById(EntryModel::class,
            $this->f3->get('PARAMS.id'));
        if ($entry) {
            $entry->purge();
            $this->f3->reroute('@home');
            return;
        }
        $this->f3->error(404, 'Entry not found');
    }

    public function fetch()
    {
        $entry = Config::getDbAdapter()->getEntityById(EntryModel::class,
            $this->f3->get('PARAMS.id'));
        if ($entry && $entry->url) {
            $entry->fetchFromUrl();
            $entry->persist();
            $this->f3->reroute('@home');
            return;
        }
        $this->f3->error(404, 'Entry not found');
    }
}