<?php

namespace App\Controller;

use App\Config;
use App\Util;
use App\Data\Entry;

class Index extends Base
{

    protected $size = 10;

    protected $page = 1;

    protected function prepare(string $baseUrl, string $title): array
    {
        $this->page = max((int)$this->f3->get('PARAMS.page'), 1);
        $data = [
            'baseurl' => $baseUrl,
            'title' => $title,
            'page' => $this->page,
            'query' => '',
        ];
        return $data;
    }

    public function list()
    {
        $data = $this->prepare('/', 'List of entries');
        $count = Config::getDbAdapter()->getEntitiesCount(\App\Data\Entry::class);
        $offset = ($this->page - 1) * $this->size;
        $data['maxpage'] = ceil($count / $this->size);
        $data['entries'] = Config::getDbAdapter()->getEntities(Entry::class, $offset, $this->size, true);
        Util::view('list', $data);
    }

    public function search()
    {
        $data = $this->prepare('/search/', 'Search results');
        $data['query'] = $this->f3->get('GET.q', '');
        if ($data['query'] === '') {
            $url = '/';
            if ($this->page > 1) {
                $url .= $this->page;
            }
            $this->f3->reroute('/');
        }
        $data['entries'] = Config::getDbAdapter()->queryEntities(Entry::class, $data['query']);
        $count = count($data['entries']);
        $offset = ($this->page - 1) * $this->size;
        $data['maxpage'] = ceil($count / $this->size);
        $data['entries'] = array_slice($data['entries'], $offset, $this->size);
        
        Util::view('list', $data);
    }

}