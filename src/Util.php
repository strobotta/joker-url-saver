<?php

namespace App;

/**
 * Utility functions for the project.
 */
class Util
{
    /**
     * Generate a unique id according to RFC 4122.
     * @param $data
     * @return string|void
     * @throws \Exception
     */
    public static function guidv4($data = null) {
        // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);

        // Set version to 0100
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        // Set bits 6-7 to 10
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

        // Output the 36 character UUID.
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    /**
     * Render and output the view.
     * @param string $name
     * @param array $data
     * @param string $layout
     * @return void
     */
    public static function view(string $name, array $data = [], string $layout = 'default')
    {
        $layoutFile = 'layout' . DIRECTORY_SEPARATOR . $layout . '.html';
        $contentFile = $name . '.html';
        $f3 = \Base::instance();
        foreach (\array_keys($data) as $key) {
            $f3->set($key, $data[$key]);
        }
        $f3->set('__content', $contentFile);
        \Template::instance()->render($contentFile);
        echo \Template::instance()->render($layoutFile);
    }

    /**
     * Guess content-type from file suffix.
     * @param string $file
     * @return string
     */
    public static function guessContentType(string $file): string
    {
        $p = strrpos($file, '.');
        if ($p) {
            $ext = strtolower(substr($file, $p + 1));
            if (\in_array($ext, ['png', 'gif', 'jpg', 'jpeg', 'webp'])) {
                return 'image/' . $ext;
            }
            if ($ext === 'svg') {
                return 'image/svg+xml';
            }
        }
        return 'application/octet-stream';
    }

    /**
     * Split a query string into single terms splitted by a space. Respect doulbe quoted terms.
     * @param string $query
     * @return string[]
     */
    public static function splitQuery(string $query): array
    {
        $parts = [];
        do {
            $p = strpos($query, '"');
            if ($p === false) {
                $parts[] = [0, $query];
                break;
            }
            if ($p > 0) {
                $parts[] = [0, substr($query, 0, $p)];
            }
            $query = substr($query, $p + 1);
            $p = strpos($query, '"');
            if ($p === false) {
                $parts[count($parts) - 1][1] .= '"' . $query;
                break;
            }
            $parts[] = [1, substr($query, 0, $p)];
            $query = substr($query, $p + 1);
        } while (mb_strlen($query) > 0);
        $result = [];
        foreach ($parts as $i => $part) {
            if ($part[0] === 0) {
                $result = array_merge($result, array_map(fn($v) => trim($v), explode(' ', $part[1])));
            } else {
                $result[] = $part[1];
            }
        }
        $result = array_filter($result, fn($v) => !empty($v));
        return array_values($result);
    }
}