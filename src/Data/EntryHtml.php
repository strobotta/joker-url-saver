<?php

namespace App\Data;

use App\Config;

/**
 * The HTML text of an entry.
 */
class EntryHtml implements Entity
{
    /**
     * @var Entry
     */
    private $entry;

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $html;

    /**
     * @var array
     */
    private $images;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id ?? '';
    }

    /**
     * @param string $id
     * @return void
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return Entry
     */
    public function getEntry(): Entry
    {
        if ($this->entry === null) {
            $this->entry = Config::getDbAdapter()->getEntityById(Entry::class, $this->id);
        }
        return $this->entry;
    }

    /**
     * Set the html text.
     * @param string $html
     * @return void
     */
    public function setHtml(string $html)
    {
        $this->html = $html;
    }

    /**
     * Get the html text.
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html ?? '';
    }

    /**
     * Persist data permanently in store.
     * @return bool
     */
    public function persist(): bool
    {
        return Config::getDbAdapter()->persist($this);
    }

    /**
     * Purge data permanently from store.
     * @return bool
     */
    public function purge(): bool
    {
        return Config::getDbAdapter()->purge($this);
    }

    /**
     * Array representation of object.
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'html' => $this->html,
        ];
    }

    public function getImages(): array
    {
        return $this->images ?? [];
    }

    public function setImages(array $images): self
    {
        $this->images = $images;
        return $this;
    }
}