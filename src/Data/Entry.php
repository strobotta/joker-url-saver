<?php

namespace App\Data;

use App\Config;
use App\UrlSaver;
use App\Util;

/**
 * Entry represents a stored article with metadata.
 */
class Entry implements Entity
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $time;

    /**
     * @var array
     */
    private $data;

    /**
     * @var EntryHtml
     */
    private $html;

    /**
     * Generic getter method.
     * @param string $property
     * @return void
     */
    public function __get($property) {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Generic setter method.
     * @param $property
     * @param $value
     * @return void
     */
    public function __set($property, $value) {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    /**
     * Get id of object.
     * @return string
     */
    public function getId(): string {
        if (!$this->id) {
            $this->id = Util::guidv4();
        }
        return $this->id;
    }

    /**
     * Array representation of the object.
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'id' => $this->getId(),
            'url' => $this->url,
            'time' => $this->time,
            'data' => $this->data,
            'html' => null,
        ];
        // Lazy loading, that's why we check first before using it.
        if ($this->html !== null) {
            $data['html'] = $this->html->toArray();
        }
        return $data;
    }

    /**
     * Return the timestamp as a formatted string.
     * @return int|string
     */
    public function getDate(string $format = null) {
        if ($format) {
            return date($format, $this->time);
        }
        return $this->time;
    }

    /**
     * Get title string from meta data.
     * @return string
     */
    public function getTitle(): string
    {
        return $this->data['title'] ?? '';
    }

    /**
     * Get the Html object.
     * @return EntryHtml
     */
    public function getHtml(): EntryHtml
    {
        if ($this->html === null) {
            $this->html = Config::getDbAdapter()->getEntityById(EntryHtml::class, $this->getId());
            if (!$this->html) {
                $this->html = new EntryHtml();
                $this->html->setId($this->getId());
            }
        }
        return $this->html;
    }

    /**
     * Set the Html object.
     * @param EntryHtml $html
     * @return void
     */
    public function setHtml(EntryHtml $html)
    {
        $this->html = $html;
    }

    /**
     * Persist object in datastore.
     * @return bool
     */
    public function persist(): bool
    {
        return Config::getDbAdapter()->persist($this);
    }

    /**
     * Purge object from datastore.
     * @return bool
     */
    public function purge(): bool
    {
        return Config::getDbAdapter()->purge($this);
    }

    /**
     * Fetch url and store result in object.
     * @return bool
     */
    public function fetchFromUrl(): bool
    {
        if ($this->url) {
            $urlSaver = new UrlSaver($this->url);
            if ($urlSaver->fetch()->success()) {
                $this->getHtml()->setHtml($urlSaver->getHtml());
                $this->getHtml()->setImages($urlSaver->getImages());
                $this->data = $urlSaver->getResult();
                unset($this->data['html']);
                $this->time = time();
                return true;
            }
        }
        return false;
    }
}