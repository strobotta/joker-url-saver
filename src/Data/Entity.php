<?php

namespace App\Data;

/**
 * Represents a business entity that is used in the application and can be
 * persisted into the database using the Adapter interface.
 */
interface Entity
{
    /**
     * Get the id of the object.
     * @return string
     */
    public function getId(): string;

    /**
     * Persist the object in the datastore.
     * @return bool
     */
    public function persist(): bool;

    /**
     * Purge object from the datastore.
     * @return bool
     */
    public function purge(): bool;

    /**
     * Array representation of the object, extract member variables.
     * @return array
     */
    public function toArray(): array;
}