<?php

namespace App;

/**
 * Config object for the application. Feed by a settings array.
 */
class Config
{
    /**
     * @var array
     */
    private static $settings;

    /**
     * Inject the data from the settings array.
     * @param array $data
     * @return void
     */
    public static function create(array $data)
    {
        self::$settings = $data;
    }

    /**
     * Get data directory on file system.
     * @return string
     */
    public static function getDataDir(): string
    {
        return self::$settings['data_dir'] ?? '.';
    }

    /**
     * Get class name of data storage that implements the App\Db\Adapter interface.
     * @return mixed
     */
    public static function getDbAdapter()
    {
        return self::$settings['db_adapter']::getInstance();
    }
}