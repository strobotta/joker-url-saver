<?php

namespace App\Db;

use App\Data\Entity;

/**
 * Implementations of this interface should be able to store the business entities (e.g. that
 * implement App\Data\Entity) into a database to have the data permanent.
 */
interface Adapter
{
    public const AND = 1;
    public const OR = 2;

    /**
     * Get the instance that implements this interface.
     * @return Adapter
     */
    public static function getInstance(): Adapter;

    /**
     * Get an array of entrities.
     * @param string $entityName
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function getEntities(string $entityName, int $offset = 0, int $limit = -1): array;

    /**
     * Returns number of all records.
     * @param string $entityName
     * @return int
     */
    public function getEntitiesCount(string $entityName): int;

    /**
     * Persist an entity in the database storage.
     * @param Entity $entity
     * @return bool
     */
    public function persist(Entity $entity): bool;

    /**
     * Purge an entity from the database storage.
     * @param Entity $entity
     * @return mixed
     */
    public function purge(Entity $entity): bool;

    /**
     * Get an entity object based on id.
     * @param string $entityName
     * @param string $id
     * @return Entity|null
     */
    public function getEntityById(string $entityName, string $id): ?Entity;

    /**
     * Query the database for entities.
     * @param string $entityName
     * @param string $query
     * @param ?int $junction
     * @return Entity[]
     */
    public function queryEntities(string $entityName, string $query, ?int $junction = self::AND): array;

}