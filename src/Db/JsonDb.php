<?php

namespace App\Db;

use App\Config;
use App\Data\Entity;
use App\Data\Entry;
use App\Data\EntryHtml;
use App\Util;

/**
 * Simple database adapter that stored the data into a JSON file and the downloaded html
 * into files.
 */
class JsonDb implements Adapter
{
    /**
     * @var Adapter
     */
    private static $instance;

    /**
     * @var array
     */
    private $db;

    /**
     * Get the current instance.
     * @return Adapter
     */
    public static function getInstance(): Adapter {
        if (static::$instance === null) {
            static::$instance = new self();
        }
        return static::$instance;
    }

    /**
     * Get the location of the json file.
     * @return string
     */
    private function getDbFile(): string
    {
        return Config::getDataDir() . DIRECTORY_SEPARATOR . 'db.json';
    }

    /**
     * Read json file and decode array.
     * @return array
     */
    private function getDb(): array
    {
        if ($this->db === null) {
            $this->db = json_decode(@file_get_contents($this->getDbFile()), true);
            if (!\is_array($this->db)) {
                $this->db = [];
            }
        }
        return $this->db;
    }

    /**
     * Persist array into json file.
     * @param array $db
     * @return void
     */
    private function setDb(array $db)
    {
        $this->db = $db;
        file_put_contents($this->getDbFile(), json_encode($db));
    }

    /**
     * Get a list of entities.
     * @param string $entityName
     * @param int $offset
     * @param int $limit
     * @param bool $reverse
     * @return array
     */
    public function getEntities(string $entityName, int $offset = 0, int $limit = -1, bool $reverse = false): array
    {
        if ($limit === -1) {
            $limit = null;
        }
        if ($entityName === Entry::class) {
            if ($reverse) {
                $chunk = \array_slice(\array_reverse($this->getDb(), true), $offset, $limit, true);
            } else {
                $chunk = \array_slice($this->getDb(), $offset, $limit, true);
            }
            \array_walk($chunk, function(&$v, $k) { $v = $this->populateEntry($k, $v); });
            return $chunk;
        }
        return [];
    }

    /**
     * Returns number of entries.
     * @param string $entityName
     * @return int
     */
    public function getEntitiesCount(string $entityName): int
    {
        if ($entityName === Entry::class) {
            return count($this->getDb());
        }
        return 0;
    }

    /**
     * Persist an entity in the data store.
     * @param Entity $entity
     * @return bool
     */
    public function persist(Entity $entity): bool
    {
        if ($entity instanceof Entry) {
            $db = $this->getDb();
            $db[$entity->getId()] = $entity->toArray();
            unset($db[$entity->getId()]['html'], $db[$entity->getId()]['id']);
            $entity->getHtml()->persist();
            $this->setDb($db);
            return true;
        } elseif ($entity instanceof EntryHtml) {
            $html = $entity->getHtml();
            $prefix = Config::getDataDir() . $entity->getId();
            if (!empty($entity->getImages())) {
                if (!is_dir($prefix)) {
                    mkdir($prefix, 0755);
                    if (!is_dir($prefix)) {
                        throw new \RuntimeException('could not create image dir: ' . $prefix);
                    }
                }
                $tempdir = '';
                $imgData = [];
                foreach ($entity->getImages() as $image) {
                    if (!\key_exists('fs', $image)) continue;
                    $img = basename($image['fs']);
                    $tempdir = substr($image['fs'], 0, -1 - strlen($img));
                    if (!rename($image['fs'], $prefix . DIRECTORY_SEPARATOR . $img)) {
                        continue;
                    }
                    $html = str_replace($image['fs'],
                        \Base::instance()->alias('entry', "id={$entity->getId()},action=img")
                        . '?file=' . urlencode($img), $html);
                    $image['fs'] = $prefix . DIRECTORY_SEPARATOR . $img;
                    $imgData[] = $image;
                }
                // Remove temp dir.
                if ($tempdir) {
                    @rmdir($tempdir);
                }
                file_put_contents($prefix . DIRECTORY_SEPARATOR . 'data.json', json_encode($imgData));
            }
            file_put_contents($prefix . '.html', $html);
        }
        return false;
    }

    /**
     * Purge an entity from the data store.
     * @param Entity $entity
     * @return bool
     */
    public function purge(Entity $entity): bool
    {
        if ($entity instanceof Entry) {
            $db = $this->getDb();
            if (isset($db[$entity->getId()])) {
                $entity->getHtml()->purge();
                unset($db[$entity->getId()]);
                $this->setDb($db);
                return true;
            }
        } elseif ($entity instanceof EntryHtml) {
            @unlink(Config::getDataDir() . $entity->getId() . '.html');
        }
        return false;
    }

    /**
     * Get a business entity by id.
     * @param string $entityName
     * @param string $id
     * @return Entity|null
     */
    public function getEntityById(string $entityName, string $id): ?Entity
    {
        if ($entityName === Entry::class) {
            $data = $this->getDb()[$id] ?? null;
            if ($data) {
                return $this->populateEntry($id, $data);
            }
            return null;
        }
        if ($entityName === EntryHtml::class) {
            $entryHtml = new EntryHtml();
            $entryHtml->setId($id);
            $entryHtml->setHtml(@file_get_contents(Config::getDataDir() . $id . '.html') ?: '');
            $imgData = @file_get_contents(Config::getDataDir() . $id . DIRECTORY_SEPARATOR . 'data.json');
            if ($imgData !== false) {
                $images = json_decode($imgData, true);
                if (\is_array($images)) {
                    $entryHtml->setImages($images);
                }
            }
            return $entryHtml;
        }
        return null;
    }

    /**
     * Populate an entry object from an array that was retrieved from the database.
     * @param string $id
     * @param array $data
     * @return Entry
     */
    private function populateEntry(string $id, array $data)
    {
        $entry = new Entry();
        $entry->id = $id;
        $entry->url = $data['url'] ?? null;
        $entry->time = $data['time'] ?? time();
        $entry->data = $data['data'] ?? [];
        return $entry;
    }

    /**
     * Query entities by field title for matching value.
     *
     * @param string $entityName
     * @param string $query
     * @param ?int $junction
     * @return Entity[]
     */
    public function queryEntities(string $entityName, string $query, ?int $junction = self::AND): array
    {
        if ($entityName !== Entry::class) {
            return [];
        }
        $data = [];
        $terms = Util::splitQuery($query);
        foreach ($this->getDb() as $id => $entry) {
            $matches = [];
            foreach ($terms as $term) {
                if (stripos($entry['data']['title'] ?? '', $term) !== false) {
                    if ($junction === self::OR) {
                        $data[$id] = $this->populateEntry($id, $entry);
                        continue 2;
                    }
                    $matches[] = true;
                }
            }
            if (count($matches) === count($terms)) {
                $data[$id] = $this->populateEntry($id, $entry);
            }
        }
        return $data;
    }
}