<?php

use App\Util;
use PHPUnit\Framework\TestCase;

class UtilTest extends TestCase
{
    public function testSplitQuery(): void
    {
        $this->assertEquals(['foo'], Util::splitQuery('foo'));
        $this->assertEquals(['foo'], Util::splitQuery('foo  '));
        $this->assertEquals(['foo'], Util::splitQuery(' foo'));
        $this->assertEquals(['foo'], Util::splitQuery(' foo  '));
        $this->assertEquals(['foo', 'bar'], Util::splitQuery('foo bar'));
        $this->assertEquals(['foo bar'], Util::splitQuery('"foo bar"'));
        $this->assertEquals(['is', 'footer empty', 'bas'], Util::splitQuery('is "footer empty" bas'));
        $this->assertEquals(['is', 'footer empty', 'bas'], Util::splitQuery('is"footer empty" bas'));
        $this->assertEquals(['is', 'footer empty', 'bas'], Util::splitQuery('is "footer empty"bas'));
        
    }
}