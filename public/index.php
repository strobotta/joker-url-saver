<?php

require_once '..' . DIRECTORY_SEPARATOR . 'boot.php';

$f3 = \Base::instance();

// Disable the deprecated messages, because the Guzzle dependency sticks to an older version
// and Graby uses utf8_encode().
error_reporting(E_ALL ^ E_DEPRECATED);

$f3->set('UI', APP_BASE_DIR . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR);
$f3->set('TEMP', APP_BASE_DIR . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR);
$f3->route('GET @home: /','\App\Controller\Index->list');
$f3->route('GET @home_paging: /@page','\App\Controller\Index->list');
$f3->route('GET @entry: /entry/@id/@action', '\App\Controller\Entry->@action');
$f3->route('GET @search: /search', '\App\Controller\Index->search');
$f3->route('GET @search_paging: /search/@page', '\App\Controller\Index->search');
$f3->route('POST @entry_create: /entry/create', '\App\Controller\Entry->create');
$f3->run();
